// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "CellShader"
{
	Properties
	{
		_ToonRamp("ToonRamp", 2D) = "white" {}
		_offset("offset", Float) = 0.5
		_Normal("Normal", 2D) = "white" {}
		[Toggle(_DYNAMICRAMP_ON)] _dynamicramp("dynamic ramp?", Float) = 1
		_RampA("RampA", Float) = -0.8
		_Float0("Float 0", Float) = -0.3
		_Albedo("Albedo", 2D) = "white" {}
		_Pattern("Pattern", 2D) = "white" {}
		_tint("tint", Color) = (0.5188679,0.5188679,0.5188679,0)
		_blinpower("blinpower", Range( 0 , 1)) = 0
		_rimOffset("rimOffset", Float) = 1
		_ColorRim("ColorRim", Color) = (0.1093361,0.5636307,0.8584906,0)
		_Tiling("Tiling", Vector) = (0.3,0.4,0,0)
		_LOLWTFCASHMONe("LOL WTF ??? CASH MONe", Float) = 1
		_PatternMix("PatternMix", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#pragma shader_feature _DYNAMICRAMP_ON
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float4 screenPos;
			float2 uv_texcoord;
			float3 worldNormal;
			INTERNAL_DATA
			float3 worldPos;
		};

		struct SurfaceOutputCustomLightingCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			Input SurfInput;
			UnityGIInput GIData;
		};

		uniform float4 _tint;
		uniform sampler2D _Pattern;
		uniform float2 _Tiling;
		uniform sampler2D _Albedo;
		uniform float4 _Albedo_ST;
		uniform sampler2D _Normal;
		uniform float4 _Normal_ST;
		uniform float _LOLWTFCASHMONe;
		uniform float _PatternMix;
		uniform sampler2D _ToonRamp;
		uniform float _offset;
		uniform float _RampA;
		uniform float _Float0;
		uniform float _rimOffset;
		uniform float _blinpower;
		uniform float4 _ColorRim;

		inline half4 LightingStandardCustomLighting( inout SurfaceOutputCustomLightingCustom s, half3 viewDir, UnityGI gi )
		{
			UnityGIInput data = s.GIData;
			Input i = s.SurfInput;
			half4 c = 0;
			#ifdef UNITY_PASS_FORWARDBASE
			float ase_lightAtten = data.atten;
			if( _LightColor0.a == 0)
			ase_lightAtten = 0;
			#else
			float3 ase_lightAttenRGB = gi.light.color / ( ( _LightColor0.rgb ) + 0.000001 );
			float ase_lightAtten = max( max( ase_lightAttenRGB.r, ase_lightAttenRGB.g ), ase_lightAttenRGB.b );
			#endif
			#if defined(HANDLE_SHADOWS_BLENDING_IN_GI)
			half bakedAtten = UnitySampleBakedOcclusion(data.lightmapUV.xy, data.worldPos);
			float zDist = dot(_WorldSpaceCameraPos - data.worldPos, UNITY_MATRIX_V[2].xyz);
			float fadeDist = UnityComputeShadowFadeDistance(data.worldPos, zDist);
			ase_lightAtten = UnityMixRealtimeAndBakedShadows(data.atten, bakedAtten, UnityComputeShadowFade(fadeDist));
			#endif
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			float4 NormalMap16 = tex2D( _Normal, uv_Normal );
			float3 ase_worldPos = i.worldPos;
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = Unity_SafeNormalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float dotResult2 = dot( normalize( (WorldNormalVector( i , NormalMap16.rgb )) ) , ase_worldlightDir );
			float NormalDirectLight7 = dotResult2;
			float2 temp_cast_7 = ((NormalDirectLight7*_offset + _offset)).xx;
			float dotResult42 = dot( normalize( (WorldNormalVector( i , NormalMap16.rgb )) ) , ase_worldlightDir );
			float lerpResult46 = lerp( ( 1.0 - step( dotResult42 , _RampA ) ) , ( 1.0 - step( dotResult42 , _Float0 ) ) , 0.5);
			float4 temp_cast_9 = (lerpResult46).xxxx;
			#ifdef _DYNAMICRAMP_ON
				float4 staticSwitch32 = temp_cast_9;
			#else
				float4 staticSwitch32 = tex2D( _ToonRamp, temp_cast_7 );
			#endif
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float4 transform185 = mul(unity_ObjectToWorld,float4( 0,0,0,1 ));
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float4 tex2DNode57 = tex2D( _Albedo, uv_Albedo );
			float4 lerpResult191 = lerp( tex2D( _Pattern, ( ( ase_screenPosNorm * float4( _Tiling, 0.0 , 0.0 ) ) * distance( ( float4( _WorldSpaceCameraPos , 0.0 ) - transform185 ) , float4( 0,0,0,0 ) ) ).xy ) , tex2DNode57 , ( NormalDirectLight7 * _LOLWTFCASHMONe ));
			float4 lerpResult196 = lerp( lerpResult191 , tex2DNode57 , _PatternMix);
			float4 Albedo60 = ( _tint * lerpResult196 );
			float4 shadow11 = ( staticSwitch32 * Albedo60 );
			#if defined(LIGHTMAP_ON) && ( UNITY_VERSION < 560 || ( defined(LIGHTMAP_SHADOW_MIXING) && !defined(SHADOWS_SHADOWMASK) && defined(SHADOWS_SCREEN) ) )//aselc
			float4 ase_lightColor = 0;
			#else //aselc
			float4 ase_lightColor = _LightColor0;
			#endif //aselc
			UnityGI gi72 = gi;
			float3 diffNorm72 = WorldNormalVector( i , NormalMap16.rgb );
			gi72 = UnityGI_Base( data, 1, diffNorm72 );
			float3 indirectDiffuse72 = gi72.indirect.diffuse + diffNorm72 * 0.0001;
			float4 Lighting71 = ( shadow11 * ( ase_lightColor * float4( ( ase_lightAtten + indirectDiffuse72 ) , 0.0 ) ) );
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float3 ase_normWorldNormal = normalize( ase_worldNormal );
			float3 ase_worldViewDir = Unity_SafeNormalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float dotResult5 = dot( ase_normWorldNormal , ase_worldViewDir );
			float ViewDirectionLight8 = dotResult5;
			float4 Rim99 = ( saturate( ( ( NormalDirectLight7 * ase_lightAtten ) * pow( ( 1.0 - saturate( ( _rimOffset + ViewDirectionLight8 ) ) ) , _blinpower ) ) ) * ( ase_lightColor * _ColorRim ) );
			c.rgb = ( Lighting71 + Rim99 ).rgb;
			c.a = 1;
			return c;
		}

		inline void LightingStandardCustomLighting_GI( inout SurfaceOutputCustomLightingCustom s, UnityGIInput data, inout UnityGI gi )
		{
			s.GIData = data;
		}

		void surf( Input i , inout SurfaceOutputCustomLightingCustom o )
		{
			o.SurfInput = i;
			o.Normal = float3(0,0,1);
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float4 transform185 = mul(unity_ObjectToWorld,float4( 0,0,0,1 ));
			float2 uv_Albedo = i.uv_texcoord * _Albedo_ST.xy + _Albedo_ST.zw;
			float4 tex2DNode57 = tex2D( _Albedo, uv_Albedo );
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			float4 NormalMap16 = tex2D( _Normal, uv_Normal );
			float3 ase_worldPos = i.worldPos;
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = Unity_SafeNormalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float dotResult2 = dot( normalize( (WorldNormalVector( i , NormalMap16.rgb )) ) , ase_worldlightDir );
			float NormalDirectLight7 = dotResult2;
			float4 lerpResult191 = lerp( tex2D( _Pattern, ( ( ase_screenPosNorm * float4( _Tiling, 0.0 , 0.0 ) ) * distance( ( float4( _WorldSpaceCameraPos , 0.0 ) - transform185 ) , float4( 0,0,0,0 ) ) ).xy ) , tex2DNode57 , ( NormalDirectLight7 * _LOLWTFCASHMONe ));
			float4 lerpResult196 = lerp( lerpResult191 , tex2DNode57 , _PatternMix);
			float4 Albedo60 = ( _tint * lerpResult196 );
			float4 temp_output_79_0 = Albedo60;
			o.Albedo = temp_output_79_0.rgb;
			o.Emission = temp_output_79_0.rgb;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardCustomLighting keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 screenPos : TEXCOORD2;
				float4 tSpace0 : TEXCOORD3;
				float4 tSpace1 : TEXCOORD4;
				float4 tSpace2 : TEXCOORD5;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				o.screenPos = ComputeScreenPos( o.pos );
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				surfIN.screenPos = IN.screenPos;
				SurfaceOutputCustomLightingCustom o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputCustomLightingCustom, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17600
401;158;1950;708;-2833.619;-668.4732;1.354973;True;False
Node;AmplifyShaderEditor.SamplerNode;15;-1904.528,-1058.881;Inherit;True;Property;_Normal;Normal;3;0;Create;True;0;0;False;0;-1;None;6b90d20704179a64c9b8efb7cefa02d9;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;16;-1594.178,-974.2776;Inherit;False;NormalMap;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;17;-2329.519,-1622.686;Inherit;False;16;NormalMap;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.WorldNormalVector;1;-1999.283,-1616.217;Inherit;False;True;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;3;-2014.247,-1416.646;Inherit;False;True;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ObjectToWorldTransfNode;185;-336.0841,-1171.625;Inherit;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WorldSpaceCameraPos;183;-361.4332,-1415.077;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.Vector2Node;188;53.37432,-1473.25;Inherit;False;Property;_Tiling;Tiling;13;0;Create;True;0;0;False;0;0.3,0.4;0.4,0.31;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.DotProductOpNode;2;-1654.514,-1513.78;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;184;-7.189179,-1217.017;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.CommentaryNode;63;-784.9047,-323.2784;Inherit;False;3111.323;1116.438;toon ramp options;20;11;10;32;46;13;49;45;14;9;47;43;39;48;44;42;34;35;33;64;66;;1,1,1,1;0;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;179;3.921552,-1732.274;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;33;-734.9047,313.8666;Inherit;False;16;NormalMap;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.DistanceOpNode;182;182.6054,-1223.776;Inherit;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;7;-1394.254,-1402.265;Inherit;False;NormalDirectLight;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;181;276.1982,-1589.326;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT2;0.3,0.4;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;186;480.0707,-1578.696;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.GetLocalVarNode;193;511.6325,-1297.612;Inherit;False;7;NormalDirectLight;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;62;716.0679,-1108.741;Inherit;False;891.7081;481.1503;albedo;5;57;59;58;60;198;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;195;540.0362,-1192.724;Inherit;False;Property;_LOLWTFCASHMONe;LOL WTF ??? CASH MONe;14;0;Create;True;0;0;False;0;1;1.09;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldNormalVector;35;-404.6685,320.3356;Inherit;False;True;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldNormalVector;4;-1962.038,-2161.188;Inherit;False;True;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;6;-1949.855,-1936.677;Inherit;False;World;True;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;34;-442.2008,534.2686;Inherit;False;True;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.DotProductOpNode;42;-183.7271,425.1949;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;194;836.5006,-1262.644;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;5;-1672.627,-2046.404;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;57;649.1056,-1089.182;Inherit;True;Property;_Albedo;Albedo;7;0;Create;True;0;0;False;0;-1;None;a541d215371d15b428811fd7c998f23c;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;189;645.2629,-1535.508;Inherit;True;Property;_Pattern;Pattern;8;0;Create;True;0;0;False;0;-1;None;00501724ba1344e409436bfeb80590fd;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;44;106.0905,432.6394;Inherit;False;Property;_RampA;RampA;5;0;Create;True;0;0;False;0;-0.8;-0.68;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;191;1104.738,-1324.385;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;8;-1524.702,-2092.974;Inherit;False;ViewDirectionLight;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;198;1075.882,-991.6603;Inherit;False;Property;_PatternMix;PatternMix;15;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;48;128.4744,644.5889;Inherit;False;Property;_Float0;Float 0;6;0;Create;True;0;0;False;0;-0.3;-0.06;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;39;-31.12984,203.7986;Inherit;True;FLOAT;1;0;FLOAT;0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.GetLocalVarNode;91;1140.778,2209.544;Inherit;False;8;ViewDirectionLight;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;93;1169.377,2091.385;Inherit;False;Property;_rimOffset;rimOffset;11;0;Create;True;0;0;False;0;1;0.65;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;43;305.7818,201.2811;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;196;1266.99,-1112.676;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StepOpNode;47;343.5105,496.6572;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;58;809.6937,-834.5905;Inherit;False;Property;_tint;tint;9;0;Create;True;0;0;False;0;0.5188679,0.5188679,0.5188679,0;0.5082325,0.764151,0.5467961,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;14;-84.909,-138.811;Inherit;False;Property;_offset;offset;2;0;Create;True;0;0;False;0;0.5;0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;9;-137.7103,-273.2784;Inherit;False;7;NormalDirectLight;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;59;1225.55,-828.6975;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;92;1386.778,2119.396;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;49;564.511,496.6574;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;13;152.5547,-234.2289;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;45;564.8096,196.843;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;77;674.1896,1248.511;Inherit;False;1449.029;603.9508;lighting;9;67;70;71;76;72;73;74;75;68;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;10;399.3845,-263.624;Inherit;True;Property;_ToonRamp;ToonRamp;1;0;Create;True;0;0;False;0;-1;6997bbd7ddbffb347abcff86c5f4e303;6997bbd7ddbffb347abcff86c5f4e303;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;60;1527.022,-833.9654;Inherit;False;Albedo;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;46;825.8109,351.0564;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;94;1568.159,2126.065;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;98;1619.197,2227.732;Inherit;False;Property;_blinpower;blinpower;10;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;108;1274.413,2450.236;Inherit;False;7;NormalDirectLight;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;75;724.1896,1680.555;Inherit;False;16;NormalMap;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;64;1390.753,-103.5893;Inherit;False;60;Albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;95;1729.535,2131.399;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LightAttenuation;110;1316.742,2680.55;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;32;1145.481,-254.489;Inherit;False;Property;_dynamicramp;dynamic ramp?;4;0;Create;True;0;0;False;0;0;1;1;True;;Toggle;2;Key0;Key1;Create;False;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;97;1906.913,2135.712;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;66;1654.706,-249.8533;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;109;1540.83,2559.791;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.IndirectDiffuseLighting;72;963.7937,1742.462;Inherit;False;Tangent;1;0;FLOAT3;0,0,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LightAttenuation;73;1034.872,1635.844;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;111;2081.807,2117.006;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;74;1262.818,1681.314;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;106;1815.632,2626.9;Inherit;False;Property;_ColorRim;ColorRim;12;0;Create;True;0;0;False;0;0.1093361,0.5636307,0.8584906,0;0.1093361,0.5636307,0.8584906,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;11;2014.031,-255.3974;Inherit;False;shadow;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LightColorNode;68;948.0781,1471.956;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.LightColorNode;103;1832.636,2442.016;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;76;1464.191,1484.922;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;104;2087.198,2515.37;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;67;1448.42,1298.511;Inherit;False;11;shadow;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;113;2254.942,2134.684;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;70;1703.662,1347.953;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;102;2472.367,2146.361;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;71;1880.219,1342.147;Inherit;False;Lighting;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;99;2657.405,2112.399;Inherit;False;Rim;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;150;3892.604,3601.683;Inherit;False;225;165;scale;1;149;;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;199;3840.788,1477.302;Inherit;False;99;Rim;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;200;3837.574,1361.06;Inherit;False;71;Lighting;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;29;-555.5709,1933.502;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;161;5186.473,3616.701;Inherit;True;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;118;2359.504,1048.483;Float;True;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;121;2693.882,1097.633;Inherit;True;True;True;False;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ObjectToWorldTransfNode;158;4216.212,4260.102;Inherit;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScreenParams;119;2321.286,1631.144;Inherit;True;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComputeScreenPosHlpNode;145;3544.699,3274.991;Inherit;False;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.OneMinusNode;28;-333.9939,1922.493;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;201;4090.807,1396.664;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;156;4304.954,3799.853;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ComponentMaskNode;154;3799.378,3910.289;Inherit;True;False;False;False;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;120;2751.187,1621.385;Inherit;True;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ComponentMaskNode;146;3816.904,3358.438;Inherit;True;False;False;False;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.UnityObjToClipPosHlpNode;153;3328.57,3724.856;Inherit;False;1;0;FLOAT3;0,0,0;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;27;-1103.1,2130.537;Inherit;False;Constant;_RampB;RampB;3;0;Create;True;0;0;False;0;0.2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;149;3935.316,3656.99;Inherit;False;Constant;_Float1;Float 1;13;0;Create;True;0;0;False;0;0.3;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceCameraPos;157;4190.863,4016.651;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.Vector3Node;151;2995.512,3717.075;Inherit;False;Constant;_Vector0;Vector 0;13;0;Create;True;0;0;False;0;0,0,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleSubtractOpNode;159;4545.107,4214.71;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TexCoordVertexDataNode;18;-1443.116,1836.923;Inherit;False;0;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;79;3939.434,940.4871;Inherit;False;60;Albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;163;5621.611,3618.477;Inherit;False;screenspace;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;22;-77.13486,1883.973;Inherit;True;myVarName;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;155;4076.436,3839.131;Inherit;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.BreakToComponentsNode;20;-1184.959,1842.15;Inherit;False;FLOAT2;1;0;FLOAT2;0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SimpleSubtractOpNode;162;4515.5,3589.369;Inherit;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.StepOpNode;23;-784.4124,1883.054;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;148;4280.73,3353.896;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.WireNode;31;-640.5836,2193.553;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComputeScreenPosHlpNode;152;3578.231,3818.365;Inherit;False;False;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.UnityObjToClipPosHlpNode;144;3342.506,3218.279;Inherit;False;1;0;FLOAT3;0,0,0;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StepOpNode;26;-783.0709,2067.403;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;147;4094.565,3278.689;Inherit;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.DistanceOpNode;160;4820.053,4147.422;Inherit;True;2;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;143;2977.982,3224.416;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;30;-1095.808,2234.933;Inherit;False;Constant;_RampC;RampC;3;0;Create;True;0;0;False;0;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;24;-1103.141,2029.388;Inherit;False;Constant;_rampA;rampA;3;0;Create;True;0;0;False;0;0.1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;203;4332.494,1028.813;Float;False;True;-1;2;ASEMaterialInspector;0;0;CustomLighting;CellShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;False;Transparent;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;16;0;15;0
WireConnection;1;0;17;0
WireConnection;2;0;1;0
WireConnection;2;1;3;0
WireConnection;184;0;183;0
WireConnection;184;1;185;0
WireConnection;182;0;184;0
WireConnection;7;0;2;0
WireConnection;181;0;179;0
WireConnection;181;1;188;0
WireConnection;186;0;181;0
WireConnection;186;1;182;0
WireConnection;35;0;33;0
WireConnection;42;0;35;0
WireConnection;42;1;34;0
WireConnection;194;0;193;0
WireConnection;194;1;195;0
WireConnection;5;0;4;0
WireConnection;5;1;6;0
WireConnection;189;1;186;0
WireConnection;191;0;189;0
WireConnection;191;1;57;0
WireConnection;191;2;194;0
WireConnection;8;0;5;0
WireConnection;39;0;42;0
WireConnection;43;0;39;0
WireConnection;43;1;44;0
WireConnection;196;0;191;0
WireConnection;196;1;57;0
WireConnection;196;2;198;0
WireConnection;47;0;39;0
WireConnection;47;1;48;0
WireConnection;59;0;58;0
WireConnection;59;1;196;0
WireConnection;92;0;93;0
WireConnection;92;1;91;0
WireConnection;49;0;47;0
WireConnection;13;0;9;0
WireConnection;13;1;14;0
WireConnection;13;2;14;0
WireConnection;45;0;43;0
WireConnection;10;1;13;0
WireConnection;60;0;59;0
WireConnection;46;0;45;0
WireConnection;46;1;49;0
WireConnection;94;0;92;0
WireConnection;95;0;94;0
WireConnection;32;1;10;0
WireConnection;32;0;46;0
WireConnection;97;0;95;0
WireConnection;97;1;98;0
WireConnection;66;0;32;0
WireConnection;66;1;64;0
WireConnection;109;0;108;0
WireConnection;109;1;110;0
WireConnection;72;0;75;0
WireConnection;111;0;109;0
WireConnection;111;1;97;0
WireConnection;74;0;73;0
WireConnection;74;1;72;0
WireConnection;11;0;66;0
WireConnection;76;0;68;0
WireConnection;76;1;74;0
WireConnection;104;0;103;0
WireConnection;104;1;106;0
WireConnection;113;0;111;0
WireConnection;70;0;67;0
WireConnection;70;1;76;0
WireConnection;102;0;113;0
WireConnection;102;1;104;0
WireConnection;71;0;70;0
WireConnection;99;0;102;0
WireConnection;29;0;23;0
WireConnection;29;1;26;0
WireConnection;29;2;31;0
WireConnection;161;0;162;0
WireConnection;161;1;160;0
WireConnection;121;0;118;0
WireConnection;145;0;144;0
WireConnection;28;0;29;0
WireConnection;201;0;200;0
WireConnection;201;1;199;0
WireConnection;156;0;155;0
WireConnection;156;1;149;0
WireConnection;154;0;152;0
WireConnection;120;0;118;0
WireConnection;120;1;119;0
WireConnection;146;0;145;0
WireConnection;153;0;151;0
WireConnection;159;0;157;0
WireConnection;159;1;158;0
WireConnection;163;0;161;0
WireConnection;22;0;28;0
WireConnection;155;0;152;0
WireConnection;155;1;154;0
WireConnection;20;0;18;0
WireConnection;162;0;148;0
WireConnection;162;1;156;0
WireConnection;23;0;20;0
WireConnection;23;1;24;0
WireConnection;148;0;147;0
WireConnection;148;1;149;0
WireConnection;31;0;30;0
WireConnection;152;0;153;0
WireConnection;144;0;143;0
WireConnection;26;0;20;0
WireConnection;26;1;27;0
WireConnection;147;0;145;0
WireConnection;147;1;146;0
WireConnection;160;0;159;0
WireConnection;203;0;79;0
WireConnection;203;2;79;0
WireConnection;203;13;201;0
ASEEND*/
//CHKSM=F46BF5337090A4F13AADD3416586D7A306A1DBCE