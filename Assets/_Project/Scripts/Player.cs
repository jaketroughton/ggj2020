﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Singleton<Player>
{
    public GameplayIntro deathThing;
    public PlayerMovement movement;
    public List<PlayerArm> playerArms = new List<PlayerArm>();
    public bool isGameOvered {get; private set;}
    public AudioClip partAttachmentSfx;
    public AudioClip armAttachmentSfx;
    public AudioClip playerDeathSfx;
    public AudioClip playerStartUpSfx;
    public ParticleSystem deathVfx;

    protected override void Awake() 
    {
        base.Awake();
    }

    void Start() 
    {
        SoundManager.Instance.PlaySound(playerStartUpSfx, transform.position);
    }

    public int GetArmPartCount()
    {
        int count = 0;
        foreach (var arm in playerArms)
        {
            count += arm.attachedParts.Count;
        }
        return count;
    }
    //Chuck yo death animation and SFX in here:
    public void DestroyPlayer() 
    {
        if (isGameOvered)
            return;
        isGameOvered = true;
        ScoreManager.Instance.SetFinalScore();
        SoundManager.Instance.PlaySound(playerDeathSfx, transform.position);
        //GameObject.Destroy(Player.Instance);
        //Application.Quit();
        ParticleManager.Instance.DoParticle("EXPLOSION", transform.position);
        deathVfx.Play();

        deathThing.StartGameOver();
        movement.enabled = false;
    }
}
