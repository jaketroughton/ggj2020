﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameplayIntro : MonoBehaviour
{
    public CanvasGroup introScreen;
    public CanvasGroup gameOverScreen;

    IEnumerator Start()
    {
        Time.timeScale = 0f;
        yield return new WaitForSecondsRealtime(3f);
        float dur = 2f;
        for (float f = 0; f < dur; f += Time.unscaledDeltaTime)
        {
            float n = f / dur;
            introScreen.alpha = 1f - n;
            Time.timeScale = n;
            yield return null;
        }
        introScreen.alpha = 0f;
        Time.timeScale = 1f;
    }

    public void StartGameOver()
    {
        StartCoroutine(GameOver());
    }

    IEnumerator GameOver()
    {
        float dur = 2f;
        for (float f = 0; f < dur; f += Time.unscaledDeltaTime)
        {
            float n = f / dur;
            gameOverScreen.alpha = n;
            Time.timeScale = 1f - n;
            yield return null;
        }
        gameOverScreen.alpha = 1f;
        Time.timeScale = 0f;
        yield return new WaitForSecondsRealtime(3f);

        SceneManager.LoadScene(0);
    }
}
