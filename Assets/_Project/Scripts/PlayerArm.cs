﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerArm : MonoBehaviour
{
    public new Rigidbody2D rigidbody2D;
    public new Collider2D collider;
    public bool attachedToPlayer;

    internal List<PlayerPart> attachedParts = new List<PlayerPart>();

    private void Update()
    {
        // only allow collisions on this arm stem if there are no
        // parts in the arm
        collider.enabled = attachedParts.Count == 0;
    }

    public void AttachPart(PlayerPart part)
    {
        attachedParts.Add(part);
        part.transform.SetParent(transform, true);
    }

    public void DetachPart(PlayerPart part)
    {
        attachedParts.Remove(part);
        if (part)
        {
            part.transform.SetParent(null);
        }
    }

    public PlayerPart LatestAttachedPart()
    {
        return attachedParts[attachedParts.Count - 1];
    }
}
