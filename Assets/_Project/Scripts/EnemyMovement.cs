﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : UpMovement
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        PlayerPart playerPart = collision.collider.GetComponent<PlayerPart>();
        if (playerPart && playerPart.attachedToPlayer)
        {
            playerPart.Detach();
            Destroy(gameObject);
        }
        else
        {
            Player player = collision.gameObject.GetComponent<Player>();
            if (player)
            {
                Player.Instance.DestroyPlayer();
                gameObject.GetComponent<Enemy>().DestroyEnemy();
                Destroy(gameObject);
            }
        }
    }
}
