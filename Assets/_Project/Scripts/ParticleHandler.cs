﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleHandler : MonoBehaviour
{
   
    private float time;
    public float life;
    void Start()
    {
        time = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if ((Time.time - time) >life) {
            Destroy(this.gameObject);
        }
    }
}
