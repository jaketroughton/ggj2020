﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UpMovement : MonoBehaviour
{
    //made this to keep it consistent maybe not needed but w/e lol
    private Transform target;
    public float moveDelta = 40f;
    private Vector3 targetDirection;

    public void Initialise(Transform target)
    {
        this.target = target;
    }

    public void SetMoveDirection(Vector3 targetEnemyDir) 
    {
        targetDirection = targetEnemyDir;
    }

    void Update()
    {
        if (target)
        {
            Vector3 targetDirection = target.position - transform.position;
            targetDirection.Normalize();
            transform.position += targetDirection * moveDelta * Time.deltaTime;
        }
        else
        {
            transform.position += targetDirection * moveDelta * Time.deltaTime;
        }
        
    }
}
