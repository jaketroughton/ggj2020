﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrusterPart : PlayerPart
{
    public float speedMultiplier = 10f;
    [SerializeField]
    private BoxCollider2D thrusterRocketCollider;
    private Timer timer;

    private bool isThrusting;
    private float thrustTime;
    public float thrustTimeMax = 1f;

    public ParticleSystem sustainedThrust;

    protected override void Start()
    {
        base.Start();
        timer = new Timer(0f);
    }

    protected override void Update()
    {
        base.Update();
        if (timer.HasTimerElapsed())
        {
            thrusterRocketCollider.enabled = false;
        }

        if (isThrusting)
        {
            thrustTime -= Time.deltaTime;
            if (thrustTime <= 0)
            {
                isThrusting = false;
                thrusterRocketCollider.enabled = false;
                sustainedThrust.Stop();
            }
            else
            {
                thrusterRocketCollider.enabled = true;
                Player.Instance.movement.rigidbody.AddForceAtPosition(transform.up * speedMultiplier, transform.position);
            }
        }
    }

    protected override void Fire()
    {
        timer = new Timer(1f);
        thrustTime = thrustTimeMax;
        sustainedThrust.Play();
        isThrusting = true;
    }
}
