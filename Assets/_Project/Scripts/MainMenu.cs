﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public Button playButton;
    public Button quitButton;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1f;
        playButton.onClick.AddListener(HandlePlay);
        quitButton.onClick.AddListener(HandleQuit);
    }

    private void HandlePlay()
    {
        SceneManager.LoadScene(1);
    }

    private void HandleQuit()
    {
        Application.Quit();
    }
}
