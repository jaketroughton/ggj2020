﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour
{

    private float time;
    public float life;
    public float speed;
    public float spinSpeed = 0.5f;
    public bool rotate;
    void Start()
    {
        time = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        this.gameObject.transform.position += Vector3.down * speed;

        if (rotate)
        {
            this.gameObject.transform.Rotate(new Vector3(0.0f, 1.0f, 0.0f), spinSpeed);
        }

        if ((Time.time - time) > life)
        {
            Destroy(this.gameObject);
        }
    }
}