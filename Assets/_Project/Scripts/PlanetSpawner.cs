﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetSpawner : MonoBehaviour
{



   public  List<GameObject> PlanetPrefabs = new List<GameObject>();
    // Start is called before the first frame update
    public new Camera camera;

    private Line spawnLine;
    private enum GameState { pick, spawn, wait }
    private GameState CurrentState;

    public string MyState;
    float LastStateChange;

    public float cooldown;
    private Vector3 spawnPoint;


    void setCurrentState(GameState State) { 

        CurrentState = State;
        MyState = State.ToString();
        LastStateChange = Time.time;
    }

    float getStateElapsed()
    {
        return Time.time - LastStateChange;
    }

    private void Awake()
    {
        // The spawn line will be along the edges of the screen
        Vector2[] points = new Vector2[2];
        points[0] = camera.ViewportToWorldPoint(new Vector2(0, 0));
        points[1] = camera.ViewportToWorldPoint(new Vector2(1, 0));
        spawnLine = new Line(points);
    }



    public void States()
    {

        switch (CurrentState)
        {

            case GameState.pick:
                spawnPoint = new Vector3(
                    //get random point between the camera points
                    spawnLine.GetPositionAtN(Random.value, out _).x,
                    transform.position.y,
                    transform.position.z);
                setCurrentState(GameState.spawn);
                break;

            case GameState.spawn:
                Instantiate(PlanetPrefabs[Random.Range(0, PlanetPrefabs.Count)], spawnPoint, Quaternion.identity);
                setCurrentState(GameState.wait);
                break;


            case GameState.wait:
                if (getStateElapsed() > cooldown) {
                    setCurrentState(GameState.pick);
                }
                break;



        }

    }




    // Update is called once per frame
    void Update()
    {
        States();
    }
}
