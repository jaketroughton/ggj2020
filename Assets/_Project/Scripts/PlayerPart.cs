﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerPart : MonoBehaviour
{
    private PlayerArm playerArm;
    private PlayerPart attachingPart; // Part that this part is using to join to the main body
    private List<PlayerPart> attachedParts = new List<PlayerPart>(); // Parts that are using this part to join the main body
    public PartLabel label;
    protected TrailRenderer trail;

    private List<Vector2> attachedList = new List<Vector2>();
    //private Dictionary<Vector2, PlayerPart> attachedList = new Dictionary<Vector2, PlayerPart>();
    public float attachOffsetMultiplier = 0.5f;

    private Vector2 targetDirection;
    private float moveDelta = 5f;
    private bool isOffscreen;
    private bool canBeOffScreen = true;

    public bool attachedToPlayer {get; private set;}
    private KeyCode controlKey = KeyCode.None;
    public float cooldownMax = 3f;
    protected float cooldownTimer;
    public AudioClip partSfx;
    public AudioClip partDestroySfx;

    public event Action OnDestroyed;

    private Vector2 targetPosition;
    private Quaternion targetRotation;

    protected virtual void Reset()
    {
    }

    protected virtual void Start()
    {

        label = GetComponentInChildren<PartLabel>();
        trail = GetComponentInChildren<TrailRenderer>();
        SetTargetPosition();
        controlKey = PartKeys.GetNextKeycode();
        string s = controlKey.ToString();
        string s1 = s.Substring(s.Length - 1);
        label.Init(s1);
        transform.eulerAngles = Vector3.forward * 360f * UnityEngine.Random.value;
    }

    public void SetTargetPosition()
    {
        Vector2 targetPlayerPosition = Player.Instance.transform.position;
        targetDirection = targetPlayerPosition - (Vector2)transform.position;
        targetDirection.Normalize();
    }

    protected virtual void Update()
    {
        cooldownTimer -= Time.deltaTime;

        if (cooldownTimer < 0f)
        {
            if (Input.GetKeyDown(controlKey) && attachedToPlayer)
            {
                SoundManager.Instance.PlaySound(partSfx, transform.position);
                Fire();
                cooldownTimer = cooldownMax;
            }
            label.SetIdle(true);
        }
        else
        {
            label.SetIdle(false);
        }

        if (!attachedToPlayer)
        {
            transform.position += (Vector3)targetDirection * moveDelta * Time.deltaTime;

            if (canBeOffScreen && Camera.main.IsWorldPointOnScreen(transform.position))
            {
                canBeOffScreen = false;
            }

            if (!canBeOffScreen && !isOffscreen && !Camera.main.IsWorldPointOnScreen(transform.position))
            {
                Destroy(gameObject, 0.5f);
                isOffscreen = true;
            }
        }
    }

    protected abstract void Fire();

    // Floating parts will collide with one another and not attach / bump away 
    // if other part is not on the player
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!attachedToPlayer)
        {
            PlayerArm otherArm = collision.collider.GetComponent<PlayerArm>();
            if (otherArm)
            {
                OrientToCollision(true, collision, out Vector2 mine, out Vector2 theirs);
                Attach(otherArm, null, mine, theirs);
                SoundManager.Instance.PlaySound(Player.Instance.armAttachmentSfx, transform.position);
            }
            else
            {
                PlayerPart otherPart = collision.collider.GetComponent<PlayerPart>();
                if (otherPart && otherPart.attachedToPlayer /*&& otherPart.playerArm && otherPart.playerArm.attachedToPlayer*/)
                {
                    //OrientToCollision(otherPart.playerArm.attachedParts.Count > 4, collision);
                    OrientToCollision(otherPart.playerArm.attachedParts.Count < 4, collision, out Vector2 mine, out Vector2 theirs);
                    Attach(otherPart.playerArm, otherPart, mine, theirs);
                    SoundManager.Instance.PlaySound(Player.Instance.partAttachmentSfx, transform.position);
                }
            }
        }
    }

    public void OrientToCollision(bool forceUp, Collision2D collision2D, out Vector2 myFullDir, out Vector2 theirFullDir)
    {
        Transform ot = collision2D.collider.transform;

        Vector2 offset = default;
        if (forceUp)
        {
            offset = ot.up;
            transform.FaceDirection(offset);
        }
        else
        {
            ContactPoint2D contactPoint2D = collision2D.GetContact(0);
            var hit = Physics2D.Raycast(transform.position, contactPoint2D.point - (Vector2)transform.position);
            if (hit.collider != null)
            {
                offset = hit.normal;
            }
            transform.rotation = ot.rotation;
        }

        transform.position = ot.position + (Vector3)offset * attachOffsetMultiplier;

        myFullDir = transform.InverseTransformPoint(ot.position).normalized.Round();
        theirFullDir = ot.InverseTransformPoint(transform.position).normalized.Round();
    }

    public void Attach(PlayerArm armAttachingTo, PlayerPart partAttachingTo, Vector2 myFullDir, Vector2 theirFullDir)
    {
        trail.gameObject.GetComponent<TrailRenderer>().enabled = false;
        if (partAttachingTo)
        {
            if (partAttachingTo.attachedList.Contains(theirFullDir))
            {
                Destroy(gameObject);
                return;
            }
            else
            {
                partAttachingTo.attachedList.Add(theirFullDir);
                attachedList.Add(myFullDir);
                partAttachingTo.attachedParts.Add(this);
            }
            attachingPart = partAttachingTo;
            attachingPart.OnDestroyed += HandleAttachingPartDestroyed;
        }

        armAttachingTo.AttachPart(this);
        attachedToPlayer = true;
        this.playerArm = armAttachingTo;
        gameObject.layer = LayerMask.NameToLayer("PlayerPartAttached");

        if (controlKey != KeyCode.None)
        {
            ScoreManager.Instance.keysLabel.text += KeyName();
        }
    }

    string KeyName()
    {
        string s = controlKey.ToString();
        string s1 = s.Substring(s.Length - 1);
        return s1;
    }

    public void Detach()
    {
        SoundManager.Instance.PlaySound(partDestroySfx, transform.position);
        playerArm.DetachPart(this);
        if (controlKey != KeyCode.None)
        {
            ScoreManager.Instance.RemoveChar(KeyName());
        }
        if (attachingPart)
            attachingPart.attachedParts.Remove(this);
        foreach (var item in attachedParts.ToArray())
        {
            item.Detach();
        }

        ParticleManager.Instance.DoParticle("EXPLOSION", transform.position);

        Destroy(gameObject);

        //
        //attachedToPlayer = false;
        //this.playerArm = null;
        //gameObject.layer = LayerMask.NameToLayer("PlayerPartLoose");
        //attachingPart.OnDestroyed -= HandleAttachingPartDestroyed;
        //attachingPart = null;
    }

    private void HandleAttachingPartDestroyed()
    {
        Destroy(gameObject);
    }
}
