﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public new Rigidbody2D rigidbody;
    public float moveStrength = 10f;

    private float hInput;
    private float vInput;



    void Update()
    {
        // The "Input" axes are set from the Edit>Project Settings menu
        hInput = Input.GetAxisRaw("Horizontal");
        vInput = Input.GetAxisRaw("Vertical");

        var viewportPos = Camera.main.WorldToViewportPoint(transform.position);
        if (viewportPos.x < 0f)
        {
            transform.position = Camera.main.ViewportToWorldPoint(new Vector3(1f, viewportPos.y, viewportPos.z));
        }
        else if (viewportPos.x > 1f)
        {
            transform.position = Camera.main.ViewportToWorldPoint(new Vector3(0f, viewportPos.y, viewportPos.z));
        }
        if (viewportPos.y < 0f)
        {
            transform.position = Camera.main.ViewportToWorldPoint(new Vector3(viewportPos.x, 1f, viewportPos.z));
        }
        else if (viewportPos.y > 1f)
        {
            transform.position = Camera.main.ViewportToWorldPoint(new Vector3(viewportPos.x, 0f, viewportPos.z));
        }

        return;

        transform.rotation = Quaternion.Euler(
            new Vector3(
                0f, 0f, AngleBetweenTwoPoints(
                 Camera.main.WorldToViewportPoint(transform.position),
                 Camera.main.ScreenToViewportPoint(Input.mousePosition)
            )
            ));


        float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
        {
            return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
        }

        //this.gameObject.transform.LookAt(new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, 0.0f, Camera.main.ScreenToWorldPoint(Input.mousePosition).z), new Vector3(1.0f,0.0f,0.0f));


    }

    private void FixedUpdate()
    {
        rigidbody.AddForce(new Vector2(hInput, vInput) * moveStrength);
    }
}
