﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ShotgunnerPart : PlayerPart
{
    private const float detectionRadius = 1f;
    [SerializeField]
    private PolygonCollider2D coneRadiusCollider;
    private Timer timer;
    private const float recoilMultiplier = 10f;

    public ParticleSystem shotEffect;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        timer = new Timer(0f);
    }

    protected override void Update()
    {
        if (timer.HasTimerElapsed())
        {
            coneRadiusCollider.enabled = false;
        }
        base.Update();
    }

    // Update is called once per frame
    protected override void Fire()
    {
        timer = new Timer(0.2f);
        coneRadiusCollider.enabled = true;
        Player.Instance.movement.rigidbody.AddForceAtPosition(-transform.up * recoilMultiplier, transform.position, ForceMode2D.Impulse);
        shotEffect.Play();
    }
}
