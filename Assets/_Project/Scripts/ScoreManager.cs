﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : Singleton<ScoreManager>
{
    public Text scoreLabel;
    public Text keysLabel;
    private float floatingScore;

    void Update()
    {
        if (!Player.Instance.isGameOvered)
        {
            floatingScore += Time.deltaTime * Player.Instance.GetArmPartCount();
        }
        SetFinalScore();
    }

    public void SetFinalScore()
    {
        scoreLabel.text = (Mathf.FloorToInt(floatingScore)).ToString();
        scoreLabel.enabled = true;
    }

    public void RemoveChar(string c)
    {
        keysLabel.text = keysLabel.text.Replace(c, string.Empty);
    }
}
