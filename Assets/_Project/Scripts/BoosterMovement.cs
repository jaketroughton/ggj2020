﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoosterMovement : UpMovement
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Player player = collision.gameObject.GetComponent<Player>();
        if (player)
        {
            Destroy(gameObject);
        }
    }
}

