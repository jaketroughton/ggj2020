﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer {

    private float timerInSeconds;

	public Timer(float timer) {
		timerInSeconds = Time.time + timer;
	}
	
	public bool HasTimerElapsed() {
		return timerInSeconds <= Time.time;
	}

	public void RefreshTimer(float timer) {
		timerInSeconds = Time.time + timer;
	}
}
