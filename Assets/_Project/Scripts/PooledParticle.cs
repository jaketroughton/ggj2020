﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PooledParticle : MonoBehaviour, IPoolable
{
    public Action OnDeactivate { get; set; }

    public GameObject GameObject => gameObject;

    public new ParticleSystem particleSystem;
    public float lifeTime = 3f;

    public void Deactivate()
    {
        GameObject.SetActive(false);
        OnDeactivate?.Invoke();
    }

    public void OnActivate()
    {
        if(particleSystem)
            particleSystem.Play();
    }

    void Update()
    {
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0f)
        {
            Deactivate();
        }
    }
}
