﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPartSlot : MonoBehaviour
{
    public PlayerPart myPart;
    //public PlayerPart slottedPart;
    public bool isFull;

    public Vector2 SlotDirection()
    {
        return (transform.position - myPart.transform.position).normalized;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(gameObject);
    }

    private void Update()
    {
        Collider2D[] res = new Collider2D[1];
        ContactFilter2D cf = new ContactFilter2D();
        cf.NoFilter();
        if (Physics2D.OverlapCollider(GetComponent<Collider2D>(), cf, res) > 0)
        {
            Destroy(gameObject);
        }
    }
}
