﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    public new Camera camera;
    public List<PlayerPart> baseParts;
    public float spawnCooldownMax = 10f;
    public float spawnCooldownMin = 5f;
    private Line spawnLine;
    private Timer timer;

    // Start is called before the first frame update
    void Start()
    {
        // The spawn line will be along the edges of the screen
        Vector2[] points = new Vector2[5];
        points[0] = camera.ViewportToWorldPoint(new Vector2(0, 0));
        points[1] = camera.ViewportToWorldPoint(new Vector2(0, 1));
        points[2] = camera.ViewportToWorldPoint(new Vector2(1, 1));
        points[3] = camera.ViewportToWorldPoint(new Vector2(1, 0));
        points[4] = camera.ViewportToWorldPoint(new Vector2(0, 0));
        spawnLine = new Line(points);
        timer = new Timer(Random.Range(spawnCooldownMin, spawnCooldownMax));
    }

    // Update is called once per frame
    void Update()
    {
        if(!timer.HasTimerElapsed())
        {
            return;
        }
        Instantiate(baseParts[Random.Range(0, baseParts.Count)], spawnLine.GetPositionAtN(Random.value, out _), Quaternion.identity);
        timer.RefreshTimer(Random.Range(spawnCooldownMin, spawnCooldownMax));
    }
}
