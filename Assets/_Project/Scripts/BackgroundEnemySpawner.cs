﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundEnemySpawner : MonoBehaviour
{
    public new Camera camera;
    public Player player;

    [Header("Enemy Spawning")]
    public Enemy enemyPrefab;
    private List<Line> spawnLines = new List<Line>();
    private float spawnTimeMin = 5f;
    private float spawnTimeMax = 10f;
    private List<Line> currentSpawningPoints = new List<Line>();
    private Timer spawnCooldownTimer;
    private Timer difficultyTimer;
    private float previousRecordedClockInSeconds = 0f;
    private float secondsToChangeDifficultyLevel = 10f;

    public List<GameObject> PlanetPrefabs = new List<GameObject>();
    private Line spawnLine;
    private enum GameState {pick, spawn, wait }
    private GameState CurrentState;

    public string MyState;
    float LastStateChange;

    public float cooldown;
    private Vector3 spawnPoint;


    void setCurrentState(GameState State)
    {

        CurrentState = State;
        MyState = State.ToString();
        LastStateChange = Time.time;
    }

    float getStateElapsed()
    {
        return Time.time - LastStateChange;
    }

    private void Awake()
    {
        // The spawn line will be along the edges of the screen
        Vector2[] points = new Vector2[4];
        points[0] = camera.ViewportToWorldPoint(new Vector2(0, 0));
        points[1] = camera.ViewportToWorldPoint(new Vector2(1, 0));

        spawnLine = new Line(points);
    }



    public void States()
    {

        switch (CurrentState)
        {

            case GameState.pick:
                spawnPoint = new Vector3(
                    //get random point between the camera points
                    spawnLine.GetPositionAtN(Random.value, out _).x,
                    transform.position.y,
                    transform.position.z);
                setCurrentState(GameState.spawn);
                break;

            case GameState.spawn:
                GameObject G = Instantiate(PlanetPrefabs[Random.Range(0, PlanetPrefabs.Count)], spawnPoint, Quaternion.identity);
                Vector3 targetEnemyPos = new Vector3(Random.Range(-1f, 1f), 1f, 0.0f);
                G.GetComponent<EnemyMovement>().SetMoveDirection(
                    targetEnemyPos);
                G.transform.FaceDirection(targetEnemyPos);
                G.transform.SetParent(this.transform);
                setCurrentState(GameState.wait);
                break;


            case GameState.wait:
                if (getStateElapsed() > cooldown)
                {
                    setCurrentState(GameState.pick);
                }
                break;
        }

    }



    private void Update()
    {
        States();

    }
}
