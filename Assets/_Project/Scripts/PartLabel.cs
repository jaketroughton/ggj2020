﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartLabel : MonoBehaviour
{
    public TextMesh textMesh;
    public Color idleColor = Color.white;
    public Color cdColor= Color.black;
    public void Init(string s)
    {
        textMesh.text = s;
    }

    public void SetIdle(bool b)
    {
        textMesh.color = b ? idleColor : cdColor;
    }

    private void Update()
    {
        transform.rotation = Quaternion.identity;
    }
}
