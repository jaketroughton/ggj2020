﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line
{
    private readonly float[] distances;
    private Vector2[] points;
    private readonly float length;

    public Line(params Vector2[] ps)
    {
        distances = new float[ps.Length];
        points = ps;
        distances[0] = 0;
        for (int i = 1; i < points.Length; i++)
        {
            length += (points[i] - points[i - 1]).magnitude;
            distances[i] = length;
        }
    }

    public Vector2 GetPositionAtN(float n, out Vector2 segment, Vector2 offset = default)
    {
        n = Mathf.Clamp01(n);
        float targetLength = length * n;
        int index = 1;
        while (targetLength > distances[index])
        {
            ++index;
        }

        segment = points[index] - points[index - 1];
        float lengthAlongSegmentDirection = targetLength - distances[index - 1];
        return points[index - 1] + segment.normalized * lengthAlongSegmentDirection + offset;
    }

    public Vector2 GetSegmentNormal(Vector2 segment, bool flip)
    {
        return (new Vector2(segment.y, -segment.x) * (flip ? -1f : 1f)).normalized;
    }

    public Vector2 GetNormalAtN(float n, bool flip)
    {
        n = Mathf.Clamp01(n);
        float targetLength = length * n;
        int index = 1;
        while (targetLength > distances[index])
        {
            ++index;
        }

        Vector2 v = points[index] - points[index - 1];

        return (new Vector2(v.y, -v.x) * (flip ? -1f : 1f)).normalized;
    }

    public void DebugDrawSegments(Color color, float duration)
    {
        for (int i = 1; i < points.Length; i++)
        {
            Debug.DrawLine(points[i], points[i - 1], color, duration);
        }
    }
}
