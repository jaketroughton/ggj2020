﻿using UnityEngine;

public class SimpleRotation : MonoBehaviour
{
    public Vector3 localEulerRate;
    private void Update()
    {
        transform.localEulerAngles += localEulerRate * Time.deltaTime;
    }
}
