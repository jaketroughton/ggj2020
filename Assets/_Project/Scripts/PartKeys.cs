﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public static class PartKeys
{
    private static int usedKeyCounter = 0;
    private static List<KeyCode> keys = new List<KeyCode>()
    {
         KeyCode.Q,
         KeyCode.W,
         KeyCode.E,
         KeyCode.R,
         KeyCode.T,
         KeyCode.Y,
         KeyCode.U,
         KeyCode.I,
         KeyCode.O,
         KeyCode.P,
         KeyCode.A,
         KeyCode.S,
         KeyCode.D,
         KeyCode.F,
         KeyCode.G,
         KeyCode.H,
         KeyCode.J,
         KeyCode.K,
         KeyCode.L,
         KeyCode.Z,
         KeyCode.X,
         KeyCode.C,
         KeyCode.V,
         KeyCode.B,
         KeyCode.N,
         KeyCode.M,
         KeyCode.Alpha1,
         KeyCode.Alpha2,
         KeyCode.Alpha3,
         KeyCode.Alpha4,
         KeyCode.Alpha5,
         KeyCode.Alpha6,
         KeyCode.Alpha7,
         KeyCode.Alpha8,
         KeyCode.Alpha9,
         KeyCode.Alpha0,
    };
    public static KeyCode GetNextKeycode()
    {
        if (usedKeyCounter == keys.Count)
            usedKeyCounter = 0;
        KeyCode key = keys[usedKeyCounter];
        ++usedKeyCounter;
        return key;
    }
}