﻿using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public new Camera camera;
    public Player player;

    [Header("Enemy Spawning")]
    public Enemy enemyPrefab;
    private float myTime;
    public float spawnCooldownStart = 6f;
    public float spawnCooldownEnd = 1f;
    private float spawnCooldown = 6f;
    private Line spawnLine;

    private void Awake()
    {
        // The spawn line will be along the edges of the screen
        Vector2[] points = new Vector2[5];
        points[0] = camera.ViewportToWorldPoint(new Vector2(0, 0));
        points[1] = camera.ViewportToWorldPoint(new Vector2(0, 1));
        points[2] = camera.ViewportToWorldPoint(new Vector2(1, 1));
        points[3] = camera.ViewportToWorldPoint(new Vector2(1, 0));
        points[4] = points[0];
        spawnLine = new Line(points);
    }

    private void Update()
    {
        myTime += Time.deltaTime;

        if (spawnCooldown > 0f)
        {
            spawnCooldown -= Time.deltaTime;
        }
        else
        {
            Enemy newEnemy = Instantiate(enemyPrefab, spawnLine.GetPositionAtN(Random.value, out Vector2 segment), Quaternion.identity);
            Vector3 lookdir = spawnLine.GetSegmentNormal(segment, false);
            newEnemy.transform.FaceDirection(lookdir);
            newEnemy.movement.SetMoveDirection(lookdir);
            spawnCooldown = Mathf.Lerp(spawnCooldownStart, spawnCooldownEnd, myTime / 60f);
        }
    }
}
