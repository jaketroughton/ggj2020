﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public EnemyMovement movement;
    private bool isOffscreen;
    private bool canBeOffScreen = true;
    public AudioSource audioSource;
    public AudioClip enemySpawnSfx;
    public AudioClip enemyDeathSfx;

    void Start()
    {
        SoundManager.Instance.PlaySound(enemySpawnSfx, transform.position);
    }

    void Update()
    {
        if (canBeOffScreen && Camera.main.IsWorldPointOnScreen(transform.position))
        {
            canBeOffScreen = false;
        }

        if (!canBeOffScreen && !isOffscreen && !Camera.main.IsWorldPointOnScreen(transform.position))
        {
            Destroy(gameObject, 0.5f);
            isOffscreen = true;
        }
    }

    public void DestroyEnemy()
    {
        SoundManager.Instance.PlaySound(enemyDeathSfx, transform.position);
        Destroy(gameObject);
    }
}
