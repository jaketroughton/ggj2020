﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Clock : Singleton<Clock>
{

    private Text clockUi;
    private float currentTimerInSeconds;

    protected override void Awake()
    {
        clockUi = gameObject.GetComponent<Text>();
        base.Awake();
    }

    void Start()
    {
        currentTimerInSeconds = 0f;
    }

    void Update()
    {
        if (!Player.Instance.isGameOvered)
        {
            Count();
            clockUi.text = GetReadableTime(); ;
        }
    }

    int GetTimerInMinutes()
    {
        return Mathf.FloorToInt((currentTimerInSeconds - Time.deltaTime) / 60f);
    }

    void Count()
    {
        currentTimerInSeconds += Time.deltaTime;
    }

    public int GetTimerInSeconds()
    {
        return Mathf.FloorToInt((currentTimerInSeconds - Time.deltaTime) % 60f);
    }

    string GetReadableTime()
    {
        return $"{GetTimerInMinutes().ToString("00")} : {GetTimerInSeconds().ToString("00")}";
    }

}
