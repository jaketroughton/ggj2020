﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPartTest : PlayerPart
{
    public List<GameObject> partlist = new List<GameObject>();
    public Transform graphicAnchor;
    
    protected override void Start()
    {
        trail = GetComponentInChildren<TrailRenderer>();
        SetTargetPosition();
        label.gameObject.SetActive(false);
        transform.eulerAngles = Vector3.forward * 360f * UnityEngine.Random.value;

        var newGraphic = Instantiate(partlist.GetRandomElement());
        newGraphic.transform.SetParent(graphicAnchor);
        newGraphic.transform.localPosition = Vector3.zero;
        newGraphic.transform.localEulerAngles = Vector3.up * 90f;
        newGraphic.transform.localScale = Vector3.one;
    }
    protected override void Fire()
    {
    }
}
