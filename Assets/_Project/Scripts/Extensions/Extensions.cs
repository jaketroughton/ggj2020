﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    public static void MoveToFacePoint(this Transform transform, Vector3 point, float speed)
    {
        float targetAngleDeg = Mathf.Atan2(point.y - transform.position.y, point.x - transform.position.x) * Mathf.Rad2Deg - 90f;
        transform.localRotation = Quaternion.RotateTowards(transform.localRotation, Quaternion.Euler(0f, 0f, targetAngleDeg), speed);
    }
    public static void MoveToFaceDirection(this Transform transform, Vector3 direction, float speed)
    {
        transform.MoveToFacePoint(transform.position + direction, speed);
    }
    public static void LerpToFacePoint(this Transform transform, Vector3 point, float speed)
    {
        float targetAngleDeg = Mathf.Atan2(point.y - transform.position.y, point.x - transform.position.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0f, 0f, targetAngleDeg), speed);
    }
    public static void LerpToFaceDirection(this Transform transform, Vector3 direction, float speed)
    {
        transform.LerpToFacePoint(transform.position + direction, speed);
    }
    public static void FacePoint(this Transform transform, Vector3 point)
    {
        float targetAngleDeg = Mathf.Atan2(point.y - transform.position.y, point.x - transform.position.x) * Mathf.Rad2Deg - 90f;
        transform.rotation = Quaternion.Euler(0f, 0f, targetAngleDeg);
    }
    public static void FaceDirection(this Transform transform, Vector3 direction)
    {
        transform.FacePoint(transform.position + direction);
    }
    public static bool IsWorldPointOnScreen(this Camera cam, Vector3 point)
    {
        Vector2 scrPoint = cam.WorldToScreenPoint(point);
        return scrPoint.x > 0 && scrPoint.x < Screen.width && scrPoint.y > 0 && scrPoint.y < Screen.height;
    }
    public static Quaternion FigurePoint(this Transform transform, Vector3 point)
    {
        float targetAngleDeg = Mathf.Atan2(point.y - transform.position.y, point.x - transform.position.x) * Mathf.Rad2Deg - 90f;
        return Quaternion.Euler(0f, 0f, targetAngleDeg);
    }
    public static Quaternion FigureDir(this Transform transform, Vector3 dir)
    {
        return transform.FigurePoint(transform.position + dir);
    }
    public static Vector3 Round(this Vector3 v)
    {
        return new Vector3(Mathf.RoundToInt(v.x), Mathf.RoundToInt(v.y), Mathf.RoundToInt(v.z));
    }

    public static T GetRandomElement<T>(this IList<T> list)
    {
        return list[Random.Range(0, list.Count)];
    }
}
