﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : Singleton<ParticleManager>
{
    public ObjectPool<PooledParticle> explosionPool;
    public PooledParticle explosionPrefab;

    public ObjectPool<PooledParticle> gunFlashPool;
    public PooledParticle gunFlashPrefab;

    public ObjectPool<PooledParticle> thrustPool;
    public PooledParticle thrustPrefab;

    protected override void Awake()
    {
        base.Awake();
        explosionPool = new ObjectPool<PooledParticle>(20);
        gunFlashPool = new ObjectPool<PooledParticle>(20);
        thrustPool = new ObjectPool<PooledParticle>(20);
        explosionPool.Populate(explosionPrefab);
        gunFlashPool.Populate(gunFlashPrefab);
        thrustPool.Populate(thrustPrefab);
    }

    public void DoParticle(string type, Vector3 position)
    {
        switch (type)
        {
            case "EXPLOSION":
                if (explosionPool.TryActivate(out PooledParticle guy))
                {
                    guy.transform.position = position;
                }
                break;
            case "GUN":
                if (gunFlashPool.TryActivate(out PooledParticle guy2))
                {
                    guy2.transform.position = position;
                }
                break;
            case "THRUST":
                if (thrustPool.TryActivate(out PooledParticle guy3))
                {
                    guy3.transform.position = position;
                }
                break;
            default:
                break;
        }
    }
}
